# Welcome to COMP120

## Do Now
Take out your laptop, but keep it closed.

If you don't have your laptop, don't worry. Bring it from now on.

## Introduction
Answer all questions within 90 seconds. Listen to the speaker.

1. What is your preferred name?
2. Where are you from?
3. What do you want to learn or do with your life?
4. What was your first computer?
5. How much programming experience do you have, if any?
6. Tell me one thing nobody else in the room knows about you.

## Smile! Take your picture
Open your laptop.

1. Go to: <https://en.gravatar.com/>
2. Enter your **school email address** and click the green `Get Your Gravatar` button.
3. Check your inbox. Gravatar will send an email to validate your address. Follow the link to confirm it.
4. Please use your Wentworth username. **Remember your password**.
5. Submit your picture. <https://en.gravatar.com/gravatars/new/webcam>
6. It's rated G. (No extra credit for X ratings.)
7. Go to: <http://bit.ly/COMP120Facebook>

## Syllabus
Go to <http://www.joeylawrance.com/COMP120>

## Lab Tomorrow
Bring your laptop. We will install all necessary software.