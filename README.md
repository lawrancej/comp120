# Course setup

## Get the software

Copy all files from the memory stick to the Desktop and then pass it on to your neighbor.

Alternatively, download the following installers:

* [MinGW](http://heanet.dl.sourceforge.net/project/mingw/Installer/mingw-get-inst/mingw-get-inst-20120426/mingw-get-inst-20120426.exe)
* [Gtk+](http://ftp.gnome.org/pub/gnome/binaries/win32/gtk+/2.24/gtk+-bundle_2.24.10-20120208_win32.zip)
* [Git](https://msysgit.googlecode.com/files/Git-1.8.0-preview20121022.exe)
* [TortoiseGit](http://tortoisegit.googlecode.com/files/TortoiseGit-1.7.15.0-32bit.msi)
* [Notepad++](http://download.tuxfamily.org/notepadplus/6.2.3/npp.6.2.3.Installer.exe)

## Create a bitbucket.org account

1. Go to <http://bitbucket.org> in a new tab (leave this open so you can follow along)
2. Click Sign Up, Free
3. Enter your information. Please use your Wentworth user name and email. Click Sign up.
4. Click `Create a new repository`. Use `COMP120` for the name. It should be a private repository, and select `C` for the language.

## Install software

Just click Next or Continue or Finish, except as noted below.

1. Run mingw-get-inst to install the C compiler. Select `MSYS Basic System` and `MinGW Developer Toolkit`
2. Run Git installer. Select `Run Git and included Unix tools from the Windows Command Prompt`
3. Run TortoiseGit installer.
4. Run the npp (Notepad++) installer.

## Verify the installation

1. Start -> All Programs -> MinGW -> MinGW shell
2. Type `git status`. If it says `fatal: not a git repository` you've installed git properly.
3. Type `gcc`. If it says `fatal error: No input files` you've installed the C compiler properly.
4. Type `ssh-keygen -t rsa`. Just push `Enter` repeatedly.
5. Start -> All Programs -> Notepad++ -> Notepad++. If it shows up, you installed Notepad++ properly.

## Clone this repository

1. In this tab (literally right above this text), click `Clone` and click `SSH` and select `HTTPS`.
2. Press Ctrl-C to copy.
3. In MinGW, right click the upper left corner. Select `Edit` -> `Paste`. Press `Enter`
4. When you press enter, you should see something like this:
```
Cloning into 'comp120'...
remote: Counting objects: 12, done.
remote: Compressing objects: 100% (10/10), done.
emote: Total 12 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (12/12), done.
```

## Send your code to your private repository

1. Go into the folder git created. Type: `cd comp120`
2. Back in bitbucket, in the new repository you created, click on `I have code I want to import`
3. Copy the second line (`git remote add origin blahblahblah`)
4. Paste into MinGW. Change `origin` to `me`
5. Type in: `git push -u me master`. It will ask for your bitbucket password. Even though you type your password, you won't see anything. 
6. Once you enter your password correctly, you should see something like this:
```
Counting objects: 9, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (7/7), 1024 bytes, done.
Total 7 (delta 2), reused 0 (delta 0)
remote: bb/acl: lawrancej is allowed. accepted payload.
To ssh://git@bitbucket.org/lawrancej/comp120.git
   7fd04b0..528d099  master -> master
```

## Share with me

1. Click the Gear in the upper right corner of Bitbucket.
2. Select `Access Management`
3. Under Users, type in `lawrancej` select `Admin` and press `Add`.

