// Mad libs
#include <stdio.h> // Input/Output
#include <string.h> // Import string functions
// Remove newlines
void remove_newlines (char *string) {
    int i; // Declare variable i outside loop
    // Examine each character in the string
    for (i = 0; i < strlen(string); i++)
        // Is the character a new line?
        if (string[i] == '\n')
            // If so, replace it with the
            // NUL character (ends the string)
            string[i] = '\0';
}

// Ask for a part of speech, and store it in string
void prompt (char *pos, char *string) {
    // Ask for the name
    printf ("Enter a %s: ", pos);
    // File get string
    fgets (string, 20, stdin);
    remove_newlines (string);
}

// Main function
int main (int argc, char ** argv) {
    char name[20]; // Strings (text)
    char adjective[20]; // Arrays of characters
    char noun[20];
    
    prompt ("name", name);
    prompt ("adjective", adjective);
    prompt ("noun", noun);

    printf ("Hello, %s. You have a really %s %s.",
    name, adjective, noun);
    return 0;
}