// Note to self: this is a hello world program.
// Single line comment: the compiler (gcc) ignores this.

/* <- This begins it
    This is a multiple line comment.
    
 This ends it -> */

// Imports standard input/output library (header file)
#include <stdio.h>

/*
    main is where all programs start.
    main is a function (takes input, and produces a result)
    argc is the first parameter (an integer) 
    argv is the second parameter (string array)
    
    int is short for integer
    char is short for character (alphanumeric, or punctuation, space)
    char * is a string of characters (text)
    char ** is an array of strings
*/
int main (int argc, char ** argv) { // opening curly brace
    // Put to the screen (print) some string
    // puts ("How you doin?\n");
    puts (argv[2]);
    // Return integer zero
    return 0;
} // closing curly brace