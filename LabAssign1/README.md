# Lab/Assignment 1: Unit Conversion

In fewer than 100 lines of code, convert among units of temperature, mass, and distance.
Deadline: Two weeks from date given.

* Temperature: Kelvin, Celsius, Fahrenheit
* Mass: Kilograms (kg), Pounds (lbs), Grams (g), Ounces (oz)
* Distance: Meters (m), Kilometers (km), Miles (miles)

## Example usage

    Unit converter.
    Enter a measurement, unit, and new unit: 1500 g kg
    1500 g = 1.5 kg

## Hints

Pick a canonical unit for temperature, mass, and distance (e.g., kelvin, grams, meters).
First convert from the starting unit to the canonical unit, then convert from the canonical unit to the new (desired) unit.
This requires much less work than converting directly between all possible pairs of units.

## Extra credit

* Additional units (e.g., inches, feet, centimeters)
* Keep prompting for units until the user enters quit
* Say `Invalid conversion` when asked to convert between incompatible units (e.g., inches to Fahrenheit) 

## Compiling work

    gcc unit-conversion.c -o unit-conversion.exe
    ./unit-conversion.exe

## Submitting work

    cd
    cd comp120
    git add LabAssign1
    git commit -m "Lab 1 submission."
    git push me lab1

