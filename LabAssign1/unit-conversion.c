// import standard input/output
#include <stdio.h>
// import string functions
#include <string.h>

int main (int argc, char ** argv) {
    float length; // Number to track the length
    char unit[20]; // String to track the unit
    printf ("Enter a length followed by a unit (e.g., 1 foot):");
    scanf ("%f%s", &length, unit);

    if (strcmp(unit,"feet") == 0 || strcmp(unit,"foot") == 0) {
        length *= 30.48; // same as length = length * 2.54
    } else if (strcmp(unit,"yards") == 0) {
        length *= 91.44;
    }
    // If unit is yards,then modify length by 91.44
    // If unit is meter, then modify length by 100
    // If unit is inches, then modify length by 2.54
    // otherwise, just say: I don't understand you!
    // Modify length so that it's equal to 2.54 times itself
    else {
        length = length * 2.54;
    }
    printf ("That's %f centimeters.",length);
}